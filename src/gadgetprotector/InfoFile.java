/*
 * File.java
 * class for new text file
 */
package gadgetprotector;

/**
 *
 * @author James Hudson
 */
public class InfoFile
{
    private int policyNo;
    private int curItem;
    private int curPrem;
    private int totItem;
    private int totPrem;
    private int okPol;
    private double avPrem;
    private int avItem;
    
    private int[] mCount = new int[12];
    
    InfoFile()
    {
        policyNo = 0;
        curItem = 0;
        curPrem = 0;
        totItem = 0;
        totPrem = 0;
        okPol = 0;
        avItem = 0;
        avPrem = 0.00;
        
        for (int a = 0; a < 12; a++) // array to hold the counters for the amount of policies each month
        {
            mCount[a] = 0;
        }
    }
    
    public void setMonths(String m) // method to update the month counters for the output
    {
        switch (m) // test for the month
                {
                    case "Jan":
                        mCount[0] = mCount[0] + 1;
                        break;
                    case "Feb":
                        mCount[1] = mCount[1] + 1;
                        break;
                    case "Mar":
                        mCount[2] = mCount[2] + 1;
                        break;
                    case "Apr":
                        mCount[3] = mCount[3] + 1;
                        break;
                    case "May":
                        mCount[4] = mCount[4] + 1;
                        break;
                    case "Jun":
                        mCount[5] = mCount[5] + 1;
                        break;
                    case "Jul":
                        mCount[6] = mCount[6] + 1;
                        break;
                    case "Aug":
                        mCount[7] = mCount[7] + 1;
                        break;
                    case "Sep":
                        mCount[8] = mCount[8] + 1;
                        break;
                    case "Oct":
                        mCount[9] = mCount[9] + 1;
                        break;
                    case "Nov":
                        mCount[10] = mCount[10] + 1;
                        break;
                    case "Dec":
                        mCount[11] = mCount[11] + 1;
                        break;
                    default:
                        break;
                }
        
    } // end of setMonths()
    
    
    public void setCurItem(int i) // method for setting the current number of items
    {
        curItem = i;
        
    } // end of setCurItems()
    
    
    public void setCurPrem(String p) // method for setting the current premium
    {
        if (!(p.charAt(0) == '-')) // while premium entry is NOT -1 (ie rejected)
        {
            curPrem = Integer.parseInt(p); // convert the String premium to an integer
        }
        else
        {
            curPrem = 0;
        }
        
    } // end of setCurPrem
    
    
    public void setTotals(char t) // method to set the payType
    {
        if ((t == 'A') && (curPrem > 0)) // test if payment method is annual
        {
            curPrem = (curPrem / 12);
        }
        
        if (curPrem > 0) // if accepted and within the search
        {
            totItem = totItem + curItem; // if accepted add to items running total
            
            totPrem = totPrem + curPrem; // if accepted add to premium running total
            
            policyNo++; // increase total no of policies
            
            okPol++; // invrease total of accepted policies
        }
        else // if rejected policy
        {
            policyNo++; // increase total no of policies
        }
        
    } // end of setTotals()
    
    
    public int getPolicyNo() // method to get the total number of policies
    {
        return policyNo;
        
    } // end of getPolicyNo()
    
    
    public int getAvItem() // method to get the average number of items
    {
        if (okPol > 0)
        {
            avItem = (int) ((totItem / (okPol * 1.0)) + 0.5); // calculate & round the average number of items for accepted policies
        }
        
        return avItem;
        
    } // end of getAvItem()
    
    
    public double getAvPrem() // method to get the average premium per month of the policies
    {
        if (okPol > 0)
            {
                avPrem = (totPrem / 100.0) / okPol; // calculate average premium for accepted policies
            }
        
        return avPrem;
        
    } // end of getAvPrem()
    
    
    public int getMCount(int mth) // method to return the amount of policies in a month
    {
        return mCount[mth];
        
    } // end of getMCount()
    
    
    public void resetCounts() // method to reset all counters
    {
        policyNo = 0;
        curItem = 0;
        curPrem = 0;
        totItem = 0;
        totPrem = 0;
        okPol = 0;
        avItem = 0;
        avPrem = 0.00;
        
        for (int a = 0; a < 12; a++)
        {
            mCount[a] = 0;
        }
        
    } // end of resetCounts()
    
} // end of class InfoFile()
