/*
 * GadgetProtector.java
 * Insurance premium calculator program prototype for Gadget Protector
 */

package gadgetprotector;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.InputMismatchException;


/**
 * @author James Hudson
 */


public class GadgetProtector
{
    static Scanner keyboard = new Scanner(System.in);
    
    static InputMismatchException errIME = new InputMismatchException();
    
    static File records = new File("policy.txt"); // construct path for policy.txt file
    
    static File archive = new File("archive.txt"); // construct path for archive.txt file
    
    public static void main(String[] args)
    {
        int option = masterMenu();
        
        selection(option);
        
        System.out.println("\nThe Policy Manager program will now close");
        System.out.println("\nGoodbye");
        System.out.println();
        
    } // end of main()
        
    
    
    static int masterMenu() // method for the main menu selection
    {
        int masterIn = 0;
        
        boolean errImeOk = false;
        
        do // loop while there is an InputMismatchException
        {
            try
            {
                System.out.println("\nMain Menu");
                System.out.println("=========");
                
                System.out.println("\nChoose an option from the menu;");
                
                System.out.println("\n1 -> Enter a new policy");
                System.out.println("2 -> Display a summary of policies");
                System.out.println("3 -> Display a summary of policies for the selected month");
                System.out.println("4 -> Find and display a policy");
                System.out.println("0 -> Exit");
                
                System.out.print("\nPlease enter your selection: ");
                masterIn = keyboard.nextInt();
                
                while ((masterIn < 0) || (masterIn > 4)) // input must be between 0 & 4
                {
                    System.out.println("\nThe selection is invalid, you must select an option between 0 and 4 from the menu;");
                    
                    System.out.println("\n1 -> Enter a new policy");
                    System.out.println("2 -> Display a summary of policies");
                    System.out.println("3 -> Display a summary of policies for the selected month");
                    System.out.println("4 -> Find and display a policy");
                    System.out.println("0 -> Exit");
                    
                    System.out.print("\nPlease enter your selection: ");
                    masterIn = keyboard.nextInt();
                }

                errImeOk = true; // set to true when valid input is received
            }
            catch (InputMismatchException errIME)
            {
                System.out.println("\nIllegal input!");
                
                System.out.println("\nYou must enter a number");
                
                keyboard.nextLine(); // clear pipe for the Scanner
            }
            
        } while (errImeOk == false);
        
        return masterIn; // return the validated menu option
        
    } // end of masterMenu()
    
    
    
    static void selection(int optionIn) // method for directing the chosen option
    {
        Policy aPol = new Policy();
        
        InfoFile aFile = new InfoFile(); // InfoFile class knows about the data required for option 2 & 3 summaries
        
        while (optionIn != 0)
        {
            if (optionIn == 1)
            {
                createPolicy(aPol);
            }
            else if (optionIn == 2)
            {
                int t = summaryMenu(optionIn);
                
                readFile(aFile, t, "null", false, " ", aPol);
                // " " is sent as the search text when options 2 or 3 are selected because this is an invalid entry for a search in the validation!
            }
            else if (optionIn == 3)
            {
                int t = summaryMenu(optionIn);
                
                String mth = selectMonth();
                
                readFile(aFile, t, mth, true, " ", aPol);
            }
            else // if option == 4
            {
                int t = summaryMenu(optionIn);
                
                String s = searchText();
                
                readFile(aFile, t, "null", false, s, aPol);
                
                System.out.print("\nPress enter to continue?");
                
                String contOk = keyboard.nextLine();
            }
            
            optionIn = masterMenu();
        }
    } // end of selection()
    
    
    
    static int summaryMenu(int opt) // method for the selection of the archive or current policies
    {
        int summaryIn = 0;
        
        boolean errImeOk = false;
        
        do // loop while there is an InputMismatchException
        {
            try
            {
                System.out.println("\nSelect an option from the menu:");
                
                System.out.println("\n1 -> Display a summary of current policies");
                System.out.println("2 -> Display a summary of archived policies");
                
                System.out.print("\nPlease enter your selection: ");
                summaryIn = keyboard.nextInt();
                
                while ((summaryIn < 1) || (summaryIn > 2)) // input must be 1 or 2
                {
                    System.out.println("\nThe selection is invalid, you must select 1 or 2 from the menu;");
                    
                    System.out.println("\n1 -> Display a summary of current policies");
                    System.out.println("2 -> Display a summary of archived policies");
                    
                    System.out.print("\nPlease enter your selection: ");
                    summaryIn = keyboard.nextInt();
                }

                errImeOk = true; // set to true when valid input is received
            }
            catch (InputMismatchException errIME)
            {
                System.out.println("\nIllegal input!");
                
                System.out.println("\nYou must enter a number");
                
                keyboard.nextLine();
            }
            
        } while (errImeOk == false);
        
        if ((opt == 2) || (opt == 3))
        {
            keyboard.nextLine();
        }
        
        return summaryIn;
        
    } // end of summaryMenu()
    
    
    
    static String selectMonth() // method to select the month to summarise
    {
        String selectIn;
        
        String s = "";
        
        boolean monthOk = false;
        
        do
        {
            System.out.print("\nPlease enter the month you would like to summarise: ");
            selectIn = keyboard.nextLine();
            
            selectIn = selectIn.trim();
            
            selectIn = selectIn.toLowerCase();
            
            int selectLen = selectIn.length();
            
            while (selectLen < 3) // input length must be greater than 3
            {
                System.out.println("\nThe entry is invalid, please enter atleast the first 3 letters of the selected month");
                System.out.print("\nTry again: ");
                selectIn = keyboard.nextLine();
                
                selectLen = selectIn.length();
            }
            
            selectIn = selectIn.concat("."); // added so that substring line will work if only three letters entered
            
            s = selectIn.substring(0, 1).toUpperCase().concat(selectIn.substring(1, 3));
            
            System.out.println(s);
            
            switch (s)
            {
                case "Jan":
                    monthOk = true;
                    break;
                case "Feb":
                    monthOk = true;
                    break;
                case "Mar":
                    monthOk = true;
                    break;
                case "Apr":
                    monthOk = true;
                    break;
                case "May":
                    monthOk = true;
                    break;
                case "Jun":
                    monthOk = true;
                    break;
                case "Jul":
                    monthOk = true;
                    break;
                case "Aug":
                    monthOk = true;
                    break;
                case "Sep":
                    monthOk = true;
                    break;
                case "Oct":
                    monthOk = true;
                    break;
                case "Nov":
                    monthOk = true;
                    break;
                case "Dec":
                    monthOk = true;
                    break;
                default:
                    monthOk = false;
                    break;
            }
            
        } while (monthOk == false);
        
        return s; // return the validated menu option
        
    } // end of selectMonth()
    
    
    
    static String searchText() // method to get the  search text for policies
    {
        keyboard.nextLine();
        
        boolean searchOk = false;
        
        System.out.print("\nEnter the search text: ");
        String searchIn = keyboard.nextLine();
        
        int searchLen = searchIn.length();
        
        if ((searchLen > 0) && (searchLen < 21))
        {
            for (int pos = 0; (pos < searchLen); pos++) // test to see if only white space is entered
            {
                char chTest = searchIn.charAt(pos);
                
                if (chTest != ' ') // test for white space
                {
                    searchOk = true; // true if not a white space
                }
            }
        }
        
        while (searchOk == false) // repeat while length not ok or if only spaces are entered
        {
            System.out.println("\nThe entry is invalid, the search text must be a minimum of 1 character long and a maximum of 20 characters");
            
            System.out.print("\nPlease enter the search text again: ");
            searchIn = keyboard.nextLine();
            
            searchLen = searchIn.length();
        
            if ((searchLen > 0) && (searchLen < 21))
            {
                for (int pos = 0; (pos < searchLen); pos++)
                {
                    char chTest = searchIn.charAt(pos);
                    
                    if (chTest != ' ')
                    {
                        searchOk = true;
                    }
                }
            }
        }
        
        searchIn = searchIn.trim();
        
        return searchIn;
        
    } // end of searchText()

    
    
    static void createPolicy(Policy aPol) // method for starting a new policy
    {
        aPol.newDate(); // set policy with the current date
        
        getName(aPol);
        
        getRef(aPol);
        
        getNo(aPol);
        
        getMax(aPol);
        
        getExcess(aPol);
        
        payAnnual(aPol);
        
        policySum(aPol);
        
        System.out.print("Press enter to continue?");
        
        String contOk = keyboard.nextLine();
        
        textOutput(aPol); // write to file
            
    } // end of createPolicy()
    
    
    
    static void getName(Policy aPol) // method for getting client name
    {
        keyboard.nextLine();
        
        System.out.print("\nEnter the client name: ");
        String nameIn = keyboard.nextLine();
        
        boolean nOk = aPol.setClientName(nameIn);
        
        while (nOk == false) // repeat while return from aPol is false
        {
            System.out.println("\nThe entry is invalid, the name must be a minimum of 1 character long and a maximum of 20 characters");
            
            System.out.print("\nPlease enter the name again: ");
            nameIn = keyboard.nextLine();
            
            nOk = aPol.setClientName(nameIn);
        }
        
    } // end of getName()
    
    
    
    static void getRef(Policy aPol) // method for getting reference number
    {
        boolean rOk;
        
        do // loop while reference entered is invalid
        {
            System.out.println("\nAll policies must have a reference number");
            
            System.out.println("The format should be [A-Z][A-Z][0-9][0-9][0-9][A-Z], for example AB123C");
            
            System.out.print("\nEnter a reference number for the policy: ");
            String refIn = keyboard.nextLine();
            
            rOk = aPol.setClientRef(refIn);
            
        } while (rOk == false);
        
    } // end of getRef()
    
    
    
    static void getNo(Policy aPol) // method for getting the number of gadgets
    {
        int noIn;
        
        boolean NoOk;
        
        boolean errImeOk = false;
        
        do // loop while there is an InputMismatchException
        {
            try
            {
                System.out.print("\nHow many gadgets are to be insured: ");
                noIn = keyboard.nextInt();
                
                NoOk = aPol.setGadgetNo(noIn);
                
                while (NoOk == false) // repeat the question if less than 1 is entered
                {
                    System.out.println("The number of gadgets to insure must be greater than 0");
                    
                    System.out.print("\nHow many gadgets are to be insured: ");
                    noIn = keyboard.nextInt();
                    
                    NoOk = aPol.setGadgetNo(noIn);
                }
                
                errImeOk = true; // when valid input is entered set to true
            }
            catch (InputMismatchException errIME)
            {
                System.out.println("\nIllegal input!");
                
                System.out.println("\nYou must enter a number");
                
                keyboard.nextLine(); // clear pipe for the Scanner
            }
            
        } while (errImeOk == false);
        
    } // end of getNo()
    
    
    
    static void getMax(Policy aPol) // method for getting the max value of gadgets
    {
        int valueIn;
        
        boolean mOk;
        
        boolean errImeOk = false;
        
        do
        {
            try
            {
                System.out.print("\nEnter the value of the most expensive gadget to be insured: £");
                valueIn = keyboard.nextInt();
                
                mOk = aPol.setMaxValue(valueIn);
                
                while (mOk == false) // loop while return from aPol is false
                {
                    System.out.println("\nThe value of the item must be greater than 0 and a whole number, eg, 963");
                    System.out.print("\nPlease enter the value again: £");
                    valueIn = keyboard.nextInt();
                    
                    mOk = aPol.setMaxValue(valueIn);
                }
                
                errImeOk = true;
            }
            catch (InputMismatchException errIME)
            {
                System.out.println("\nIllegal input!");
                
                System.out.println("\nYou must enter a number");
                
                keyboard.nextLine();
            }
            
        } while (errImeOk == false);
        
    } // end of getMax()
    
    
    
    static void getExcess(Policy aPol) // method for getting the level of excess
    {
        int excessIn;
        
        boolean eOk;
        
        boolean errImeOk = false;
        
        do
        {
            try
            {
                System.out.println("\nThe default excess is £30, The max excess is £70");
                
                System.out.println("\nEach £10 increase will reduce the premium by 5%");
                
                System.out.print("\nEnter the desired excess level: £");
                excessIn = keyboard.nextInt();
                
                eOk = aPol.setExcess(excessIn);
                
                while (eOk == false)
                {
                    System.out.println("\nThe amount entered is invalid. It must be a 30, 40, 50, 60 or 70");
                    
                    System.out.print("\nPlease enter the desired excess level again: £");
                    excessIn = keyboard.nextInt();
                    
                    eOk = aPol.setExcess(excessIn);
                }

                errImeOk = true;
            }
            catch (InputMismatchException errIME)
            {
                System.out.println("\nIllegal input!");
                
                System.out.println("\nYou must enter a number");
                
                keyboard.nextLine();
            }
            
        } while (errImeOk == false);
        
    } // end of getExcess()
    
    
    
    static void payAnnual(Policy aPol) // method for getting the payment method
    {
        int annualIn;
        
        boolean aOk;
        
        boolean errImeOk = false;
        
        do
        {
            try
            {
                System.out.println("\nPayment options are monthly (default) or annual");
                
                System.out.println("\nPaying annually gives a 10% discount on the premium");
                
                System.out.print("\nEnter 1 for monthly OR 2 for annual: ");
                annualIn = keyboard.nextInt();
                
                aOk = aPol.setAnnual(annualIn);
                
                while (aOk == false)
                {
                    System.out.println("\nThe amount entered is invalid. It must be 1 for monthly OR 2 for annual");
                    
                    System.out.print("\nPlease select an option again: ");
                    annualIn = keyboard.nextInt();
                    
                    aOk = aPol.setAnnual(annualIn);
                }
                
                errImeOk = true;
            }
            catch (InputMismatchException errIME)
            {
                System.out.println("\nIllegal input!");
                
                System.out.println("\nYou must enter a number");
                
                keyboard.nextLine();
            }
            
        } while (errImeOk == false);
        
        keyboard.nextLine(); // clear pipe for the Scanner
        
    } // end of PayAnnual()
    
    
    
    static void policySum(Policy aPol) // method to display the policy summary for an accepted quote
    {
        System.out.println();
        
        System.out.printf("%63s %n", "+==========================================+");
        
        System.out.printf("%20s %-40s %1s %n","|"," ","|");
        
        System.out.printf("%20s %8s %-22s %-8s %1s %n","|","Client:",aPol.getClientName()," ","|");
        
        System.out.printf("%20s %-40s %1s %n","|"," ","|");
        
        System.out.printf("%20s %8s %-11s %10s %-8s %1s %n","|","Date:",aPol.getDate(),"Ref:",aPol.getClientRef(),"|");
        
        System.out.printf("%20s %8s %-11s %10s %-8s %1s %n","|","Terms:",aPol.getAnnualStr(),"Items:",aPol.getGadgetsString(),"|");
        
        System.out.printf("%20s %8s £%-21.2f %-8s %1s %n","|","Excess:",aPol.getExcessPound()," ","|");
        
        System.out.printf("%20s %-40s %1s %n","|"," ","|");
        
        if ((aPol.isAccepted() == false) && (aPol.getGadgetNo() > 5) && (!aPol.getLimit().equals("Exceeded"))) // if policy rejected, these 2 text lines is altered!
        {
            System.out.printf("%20s %7s %32s %1s %n","|",aPol.getAnnualStr(),"Number of          ","|");
            
            System.out.printf("%20s %8s %-11s %10s %-8s %1s %n","|","Premium:","Rejected","Gadgets:","Exceeded","|");
        }
        else if (aPol.isAccepted() == false)
        {
            System.out.printf("%20s %7s %32s %1s %n","|",aPol.getAnnualStr(),"Limit per          ","|");
            
            System.out.printf("%20s %8s %-11s %10s %-8s %1s %n","|","Premium:","Rejected","Gadget:",aPol.getLimit(),"|");
        }
        else
        {
            System.out.printf("%20s %7s %32s %1s %n","|",aPol.getAnnualStr(),"Limit per          ","|");
            
            System.out.printf("%20s %8s £%-11.2f %9s %-8s %1s %n","|","Premium:",aPol.getPremium(),"Gadget:",aPol.getLimit(),"|");
        }
        
        System.out.printf("%20s %-40s %1s %n","|"," ","|");
        
        System.out.printf("%63s %n", "+==========================================+");
        
        System.out.println("");
        
    } // end of policySum()
    
    
    
    static void textOutput(Policy aPol) // method to print policy summary info to file
    {
        PrintWriter detail = null;
        
        try
        {
            FileWriter fw = new FileWriter(records, true);
            
            detail = new PrintWriter(fw);
        }
        catch (FileNotFoundException errFNFE)
        {
            System.out.println("\nThere is a problem with the policy.txt file!");
            System.out.println("The policy deatails have not been written.");
            
            keyboard.nextLine();
        }
        catch(IOException errIOE)
        {
            System.out.println("\nThere is a problem with the policy.txt file!");
            System.out.println("The policy deatails have not been written.");
            
            keyboard.nextLine();
        }
        detail.append(System.lineSeparator()); // start a new line
        detail.append(aPol.getArcDate()); // append year to file
        detail.append("\t" + aPol.getClientRef());
        detail.append("\t" + aPol.getGadgetNo());
        detail.append("\t" + aPol.getMaxVal());
        detail.append("\t" + aPol.getExcess());
        detail.append("\t" + aPol.getPremPence());
        detail.append("\t" + aPol.getPayTerm());
        detail.append("\t" + aPol.getClientName());
                
        detail.close();
        
    } // end of textOutput()
    
    
    
    static void readFile(InfoFile aFile, int selected, String month, boolean isMonth, String searchText, Policy aPol) // method to read from file
    {
        Scanner input = null;
        
        String text;
        
        FileReader fr;
        
        try
        {
            if (selected == 1)
            {
                fr = new FileReader(records);
            }
            else
            {
                fr = new FileReader(archive);
            }
            
            input = new Scanner(fr);
        }
        catch (FileNotFoundException errFNFE)
        {
            System.out.println("\nThere is a problem with the file!");
            
            keyboard.nextLine();
        }
        
        int counter = 0;
        
        if (" ".equals(searchText)) // this process if option 2 or 3 is selected
        {
            while (input.hasNext())
            {
                text = input.next();
                
                counter++;
                
                if (counter == 1)
                {
                    aFile.setMonths(text.substring(3, 6)); // get the month from thre read line
                    
                    if (isMonth && !month.equals(text.substring(3, 6))) // if option 3 was selected and the month of the read line does NOT match
                    {
                        input.nextLine(); // read in the rest of that line ready for next line
                        
                        counter = 0;
                    }
                }
                
                if (counter == 3)
                {
                    aFile.setCurItem(Integer.parseInt(text));
                }
                
                if (counter == 6)
                {
                    aFile.setCurPrem(text);
                }
                
                if (counter == 7) // check if annual or monthly premium
                {
                    aFile.setTotals(text.charAt(0));
                    
                    input.nextLine(); // read in the rest of that line ready for next line
                    
                    counter = 0;
                }
            }
            
            fileSum(aFile); // print summary
        }
        else // option 4 is selected
        {
            boolean printSum = false;
            
            while (input.hasNextLine())
            {
                text = input.nextLine();
                
                int tab3 = text.indexOf("\t", 19); // find the index of the tab between no of items & max value
                
                int tab4 = text.indexOf("\t", (tab3 + 1)); // find the index of the tab between max item value & excess
                
                int tab6 = text.indexOf("\t", (tab4 + 4)); // find the index of the tab between premium & payTerm
                
                printSum = searchInfo(text, searchText, tab6);
                
                if (printSum) // if to print then set fields in the policy class
                {
                    extractInfo(text, aPol, tab3, tab4, tab6); // call method to extract the info from the line
                    
                    policySum(aPol);
                }
            }
            
            if (printSum == false)
            {
                System.out.println("\nNo records have been found that match your search criteria!");
            }
        }
        input.close();
        
        aFile.resetCounts(); // resets counters in aPol to 0 for next summary
        
    } // end of readFile()
    
    
    
    static void fileSum(InfoFile aFile) // method to output the text file summary
    {
        System.out.println("");
        
        System.out.printf("%5s %-5s %-1s %n"," ","Total Number of policies:",aFile.getPolicyNo());
        
        System.out.printf("%5s %-5s %-1d %n"," ","Average number of items (Accepted policies):",aFile.getAvItem());
        
        System.out.printf("%5s %-5s %-1.2f %n"," ","Average Monthly Premium:",aFile.getAvPrem());
        
        System.out.printf("%5s %-5s %n"," ","Number of Policies per Month (inc. non-accepted):");
        
        System.out.println("");
        
        System.out.printf("%5s %-5s %-5s %-5s %-5s %-5s %-5s %-5s %-5s %-5s %-5s %-5s %-5s %n"," ","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
        
        System.out.printf("%5s %-5s %-5s %-5s %-5s %-5s %-5s %-5s %-5s %-5s %-5s %-5s %-5s %n"," ",aFile.getMCount(0),aFile.getMCount(1),aFile.getMCount(2),aFile.getMCount(3),
                aFile.getMCount(4),aFile.getMCount(5),aFile.getMCount(6),aFile.getMCount(7),aFile.getMCount(8),aFile.getMCount(9),aFile.getMCount(10),aFile.getMCount(11));
        
        System.out.println("");
        
        System.out.print("Press enter to continue?");
        
        String contOk = keyboard.nextLine();
        
    } // end of fileSum()
    
    
    
    static boolean searchInfo(String entry, String sText, int t6) // method to search policy entries in the archive files
    {
        int sLen = sText.length();
        
        String toTest = entry.substring(12, 18).concat(entry.substring(t6 + 3)); // create String of reference & name to test
        
        boolean printOk = false;
        
        int tLen = toTest.length();
        
        int lLen = tLen - (sLen - 1);
        // calculate the length of the loop to search through which will vary depending on the length of the search text!
        
        for (int x = 0; x < lLen; x++) // search through each character of the read line starting with the first character of the search text
        {
            if (printOk == false)
            {
                printOk = sText.equalsIgnoreCase(toTest.substring(x, (x + sLen)));
            }
        }
        
        return printOk;
        
    } // end of searchInfo()
    
    
    
    static void extractInfo(String line, Policy aPol, int t3, int t4, int t6) // method to send the data from an entry to Policy ready for printing
    {
        boolean sendDate = aPol.setDate(line.substring(0, 11));
        
        boolean sendRef = aPol.setClientRef(line.substring(12, 18));
        
        boolean sendItem = aPol.setGadgetNo(Integer.parseInt(line.substring(19, t3)));
        
        boolean sendVal = aPol.setMaxValue(Integer.parseInt(line.substring((t3 + 1), t4)));
        
        boolean sendExc = aPol.setExcess(Integer.parseInt(line.substring((t4 + 1), (t4 + 3))));
        
        char pTerm = line.charAt(t6 + 1);
        
        if (pTerm == 'A')
        {
            boolean sendTerm = aPol.setAnnual(2);
        }
        else
        {
            boolean sendTerm = aPol.setAnnual(1);
        }
        
        boolean sendName = aPol.setClientName(line.substring(t6 + 3));
                
    } // end of extractInfo()
    
} // end of class GadgetProtector