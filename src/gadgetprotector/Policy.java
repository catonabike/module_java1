/*
 * Policy.java
 * class for new policies
 */
package gadgetprotector;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author James Hudson
 */
public class Policy
{
    private String dateStart;
    private String clientName;
    private String clientRef;
    private int gadgetNo;
    private int maxValue;
    private int excess;
    private char payTerm;
    private boolean annual;
    private double premium;
    
    private Date date = new Date();
    private SimpleDateFormat fd = new SimpleDateFormat("dd MMM YYYY"); // format the date output
    
    
    Policy()
    {
        dateStart = fd.format(date);
        clientName = "";
        clientRef = "";
        gadgetNo = 0;
        maxValue = 0;
        excess = 30;
        payTerm = 'R';
        annual = false;
        premium = 0.0;
    }
    
    
    public String getDate() // method to return the policy start date
    {
        return dateStart;
        
    } // end of getDate()
    
    
    public boolean setDate(String dateIn) // method to set the date
    {
        boolean dateOk = false;
        
        String dIn = dateIn.replace('-', ' '); // change format of the date from archived to summary output style
        
        int spOne = dIn.indexOf(" ");
        
        int spTwo = dIn.lastIndexOf(" ");
        
        int dateLen = dateIn.length();
        
        if ((dateLen == 11) && (spOne == 2) && (spTwo == 6))
        {
             int dayS = Integer.parseInt(dIn.substring(0, 2)); // get the day to test
            
            if ((dayS > 0) && (dayS < 32))
            {
                String monS = dIn.substring(3, 6); // get month to test
                
                if (("Jan".equals(monS)) || ("Feb".equals(monS)) || ("Mar".equals(monS)) || ("Apr".equals(monS)) || ("May".equals(monS)) || ("Jun".equals(monS)) || ("Jul".equals(monS)) 
                        || ("Aug".equals(monS)) || ("Sep".equals(monS)) || ("Oct".equals(monS)) || ("Nov".equals(monS)) || ("Dec".equals(monS)))
                {
                     int yearS = Integer.parseInt(dIn.substring(7)); // get year to test
                    
                    if ((yearS > 999) && (yearS < 10000))
                    {
                        dateOk = true;
                        
                        dateStart = dIn;
                    }
                }
            }
        }
        
        return dateOk;
        
    } // end of setDate()
    
    
    public void newDate() // method to get the current date
    {
        dateStart = fd.format(date);
        
    } // end of newDate()
    
    
    public String getArcDate() // method to return date in format for archiving
    {
        String day = dateStart.substring(0, 2);
        String mon = dateStart.substring(3, 6);
        String year = dateStart.substring(7);
        
        String arcDate = (((day.concat("-").concat(mon)).concat("-")).concat(year));
        
        return arcDate;
        
    } // end of getArcDate()
    
    
    public String getClientName() // method to return the client name
    {
        return clientName;
        
    } // end of getClientName()
    
    
    public boolean setClientName(String name) // method to test & set the client name
    {
        boolean nameOk = false;
        
        int nameLen = name.length(); // get name length
        
        if ((nameLen > 0) && (nameLen < 21))
        {
            for (int pos = 0; (pos < nameLen); pos++) // test each character to see if only white space is entered
            {
                char chTest = name.charAt(pos);
                
                if (chTest != ' ') // test for white space
                {
                    nameOk = true; // true if not a white space
                    
                    clientName = name;
                }
            }
        }
        
        return nameOk;
        
    } // end of setClientName()
    
    
    public String getClientRef() // method to return the client reference number
    {
        return clientRef;
        
    } // end of getClientRef()
    
    
    public boolean setClientRef(String ref) // method to test and set the client reference number
    {
        boolean refOk = false;
        
        int refLength = ref.length();
        
        if (refLength == 6)
        {
            String refUp = ref.toUpperCase();
            
            char l1 = refUp.charAt(0); // get the letter character
            char l2 = refUp.charAt(1); // get the second character
            char n3 = refUp.charAt(2); // get the first number
            char n4 = refUp.charAt(3); // get the second number
            char n5 = refUp.charAt(4); // get the third number
            char l6 = refUp.charAt(5); // get the last character
            
            if ((l1 >= 'A' && l1 <= 'Z') && (l2 >= 'A' && l2 <= 'Z') && (n3 >= '0' && n3 <= '9') && (n4 >= '0' && n4 <= '9') &&
                    (n5 >= '0' && n5 <= '9') && (l6 >= 'A' && l6 <= 'Z')) // test that characters match the requirements
            {
                refOk = true;
                
                clientRef = refUp;
            }
        }
        
        return refOk;
        
    } // end of setClientRef()
    
    
    public int getGadgetNo() // method to return the number of gadgets to be insured
    {
        return gadgetNo;
        
    } // end of getGadgetNo()
    
    
    public String getGadgetsString() // method to output the number of gadgets as String
    {
        if (gadgetNo == 1)
        {
            return "One";
        }
        else if (gadgetNo == 2)
        {
            return "Two";
        }
        else if (gadgetNo == 3)
        {
            return "Three";
        }
        else if (gadgetNo == 4)
        {
            return "Four";
        }
        else if (gadgetNo == 5)
        {
            return "Five";
        }
        else
        {
            return Integer.toString(gadgetNo);
        }
    } // end of getGadgetString()
    
    
    public boolean setGadgetNo(int gadgets) // method to test & set the number of gadgets to be insured
    {
        boolean gadgOk = false;
        
        if (gadgets > 0)
        {
            gadgOk = true;
            
            gadgetNo = gadgets;
        }
        
        return gadgOk;
        
    } // end of setGadgetNo()
    
    
    public int getMaxVal() // method to get the max value
    {
        return maxValue;
        
    } // end of getMaxVal()
    
    
    public boolean setMaxValue(int value) // method to test and set the most expensive item
    {
        boolean valOk = false;
        
        if (value > 0)
        {
            valOk = true;
            
            maxValue = value;
        }
        
        return valOk;
        
    } // end of setMaxValue()
    
    
    public int getExcess() // method to return the excess as an integer
    {
        return excess;
        
    } // end of getExcess()
    
    
    public double getExcessPound() // method to return excess as a double
    {
        Double exOut = (excess * 1.00);
        
        return exOut;
        
    } // end of getExcessPound()
    
    
    public boolean setExcess(int exIn) // method to validate and set the desired excess
    {
        boolean excessOk = true;
        
        if (exIn <= 0)
        {
            excessOk = false;
        }
        else if (exIn < 40) // as long as the operator enters a positive number then the relevant excess level will be selected
        {
            excess = 30;
        }
        else if (exIn < 50)
        {
            excess = 40;
        }
        else if (exIn < 60)
        {
            excess = 50;
        }
        else if (exIn < 70)
        {
            excess = 60;
        }
        else
        {
            excess = 70;
        }
        
        return excessOk;
        
    } // end of setExcess()
    
    
    public boolean getAnnual() // method to return if annual or monthly payment
    {
        return annual;
        
    } // end of getAnnual()
    
    
    public String getAnnualStr() // method to return the payment type as a String
    {
        if (annual == true)
        {
            return "Annual";
        }
        else
        {
            return "Monthly";
        }
    } // end of getAnnualStr()
    
    
    public boolean setAnnual(int anIn) // method to set if annual or monthly payment
    {
        boolean annualOk = false;
        
        if (anIn == 2)
        {
            annualOk = true;
            
            payTerm = 'A';
            
            annual = true;
        }
        else if (anIn == 1)
        {
            annualOk = true;
            
            payTerm = 'M';
            
            annual = false;
        }
        
        return annualOk;
        
    } // end of setAnnual()
    
    
    public double getPremium() // method to calulate and return the premium
    {
        double ipPrem;
        
        double opPrem;
        
        if (gadgetNo == 1) // get premium level
        {
            if (maxValue <= 550)
            {
                ipPrem = 4.99;
            }
            else if (maxValue <= 800)
            {
                ipPrem = 6.15;
            }
            else
            {
                ipPrem = 7.30;
            }
        }
        else if ((gadgetNo == 2) || (gadgetNo == 3))
        {
            if (maxValue <= 550)
            {
                ipPrem = 9.99;
            }
            else if (maxValue <= 800)
            {
                ipPrem = 12.35;
            }
            else
            {
                ipPrem = 14.55;
            }
        }
        else
        {
            if (maxValue <= 550)
            {
                ipPrem = 14.99;
            }
            else if (maxValue <= 800)
            {
                ipPrem = 18.60;
            }
            else
            {
                ipPrem = 21.82;
            }
        }
        
        if (excess == 30) // apply excess discount if applicable
        {
            
        }
        else if (excess == 40)
        {
            ipPrem = (ipPrem * 0.95);
        }
        else if (excess == 50)
        {
            ipPrem = (ipPrem * 0.9);
        }
        else if (excess == 60)
        {
            ipPrem = (ipPrem * 0.85);
        }
        else
        {
            ipPrem = (ipPrem * 0.8);
        }
        
        if (annual == true) // apply annual payment discount if applicable
        {
            opPrem = ((ipPrem * 12) * 0.9);
        }
        else
        {
            opPrem = ipPrem;
        }
        
        premium = opPrem;
        
        return opPrem;
        
    } // end of getPremium()
    
    
    public int getPremPence() // method to return the premium amount in pence
    {
        int premPence = (int) ((premium * 100) + 0.5);
        
        if (isAccepted() == true)
        {
            return premPence;
        }
        else
        {
            return -1;
        }
        
    } // end of getPremPence()
    
    
    public boolean isAccepted() // method to check if policy is accepted
    {
        if ((gadgetNo > 5) || (maxValue > 1000))
        {
            payTerm = 'R';
            
            return false;
        }
        else
        {
            return true;
        }
    } // end of isAccepted()
    
    
    public String getLimit() // method for outputting the limit being applied as a String
    {
        if (maxValue <= 550)
        {
            return "550";
        }
        else if (maxValue <= 800)
        {
            return "800";
        }
        else if (maxValue <= 1000)
        {
            return "1000";
        }
        else
        {
            return "Exceeded";
        }
    } // end of getLimit()
    
    
    public char getPayTerm() // method to return the payment term for archiving
    {
        if (isAccepted() == true)
        {
            return payTerm;
        }
        else
        {
            return 'R';
        }
    } // end of getPayTerm()
    
} // end of class Policy